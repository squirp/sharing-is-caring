 function Retry-Command {
    [CmdletBinding()]
    Param(
        [Parameter(Position=0, Mandatory=$true)]
        [scriptblock]$ScriptBlock,

        [Parameter(Position=1, Mandatory=$false)]
        [int]$Maximum = 5,

        [Parameter(Position=2, Mandatory=$false)]
        [int]$Delay = 1
    )

    Begin {
        $cnt = 0
    }

    Process {
        do {
            $cnt++
            try {
                $ScriptBlock.Invoke()
                return
            } catch {
                Write-Error $_.Exception.InnerException.Message -ErrorAction Continue
                Start-Sleep -seconds $Delay
            }
        } while ($cnt -lt $Maximum)

        # Throw an error after $Maximum unsuccessful invocations. Doesn't need
        # a condition, since the function returns upon successful invocation.
        throw 'Execution failed.'
    }
}


get-service ccmexec | set-service -StartupType disabled

$tries=0
$serviceStatus = (get-service ccmexec | select status).status
while($serviceStatus -ne "Stopped"){
    # Stop the Service "SMS Agent Host" which is a Process "CcmExec.exe"
    Get-Service -Name CcmExec -ErrorAction SilentlyContinue | Stop-Service -Force -Verbose

    # Stop the Service "ccmsetup" which is also a Process "ccmsetup.exe" if it wasn't stopped in the services after uninstall
    Get-Service -Name ccmsetup -ErrorAction SilentlyContinue | Stop-Service -Force -Verbose
        
    start-sleep -Seconds 10  
    $tries = $tries + 1
    if($tries -gt 3){
    
        write-host "Not quitting easy... trying the hard way now."
        $id = Get-WmiObject -Class Win32_Service -Filter "Name LIKE 'ccmexec'" | Select-Object -ExpandProperty ProcessId

        Get-Process | Where-Object { $_.ID -eq $id } | Select-Object -First 1 | Stop-Process

    }


    write-host "waiting for the service to go from $serviceStatus to stopped"
    $serviceStatus = (get-service ccmexec | select status).status
}

if($serviceStatus -eq "Stopped"){
    Write-host "Service finally stopped, re-installing CM Client now."

    write-host "Stopping WMI..."
    net stop winmgmt /y
    Start-Sleep -Seconds 10
    net stop winmgmt /y

    $tries = 0
    $retrymsg = ""
    while((Test-Path "c:\windows\ccmsetup\ccmsetup.exe") -and ($tries -lt 5)){
        write-host "$retrymsg Uninstalling CM Client..."
        start-process -filepath "C:\windows\ccmsetup\ccmsetup.exe" -argumentlist "/uninstall"
        $tries = $tries + 1
        $retrymsg = "That didn't work... "
        Start-Sleep -Seconds 60
    }
    if((Test-Path "c:\windows\ccmsetup\ccmsetup.exe")){
        Write-Host "Looks like CM doesn't want to uninstall."
        Break
    }
    
    Retry-Command -ScriptBlock {
        Write-host 'Delete the folder of the SCCM Client installation: "C:\Windows\CCM"'
        Remove-Item -Path "$($Env:WinDir)\CCM" -Force -Recurse -Confirm:$false -Verbose

        Write-host 'Delete the folder of the SCCM Client Cache of all the packages and Applications that were downloaded and installed on the Computer: "C:\Windows\ccmcache"'
        Remove-Item -Path "$($Env:WinDir)\CCMSetup" -Force -Recurse -Confirm:$false -Verbose

        Write-host 'Delete the folder of the SCCM Client Setup files that were used to install the client: "C:\Windows\ccmsetup"'
        Remove-Item -Path "$($Env:WinDir)\CCMCache" -Force -Recurse -Confirm:$false -Verbose
    } -Maximum 3 -Delay 10

    start-sleep -Seconds 30

    Write-Host "Installing CM"
    start-process -filepath "\\yourserver\yourshare\ConfigMgrClient\ccmsetup.exe"

    Write-Host "And now we wait! In 5 minutes we Eval Machine Policy and hope for the best."
    start-sleep -seconds 300
    powershell -NoProfile -WindowStyle Hidden -ExecutionPolicy Bypass -Command "Invoke-WmiMethod -Namespace root\CCM -Class SMS_Client -Name TriggerSchedule '{00000000-0000-0000-0000-000000000021}';"
    write-host "Done."
} 
